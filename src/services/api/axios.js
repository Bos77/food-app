import axios from "axios";

export const request = axios.create({
  method: "get",
  baseURL: "https://cors-anywhere.herokuapp.com/https://burhoncoder.github.io",
  headers: { "X-Custom-Header": "Burxon food app" },
});
